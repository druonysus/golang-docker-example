run: build
	docker run -it --rm hello:latest

build-bin: 
	GOOS=linux go build -v -o ./bin/hello ./src/hello.go

import:
	cd ./bin && tar -c . | docker import -c 'ENTRYPOINT ["./hello"]' - hello:latest

build-docker: build-bin
	docker build --rm -t hello:latest .

build: build-bin import

