golang-docker-example
===

This repo shows to demenstrate how simple it is to build a docker container from just a basic Go binary. The long-and-short of it is that because Go always produces a staticly-linked binary, there are *zero* dependancies, and so, in the case where the binary is not looking for specifc files in a specificly laidout filesystem, you simply don't need anything other than the binary inside the docker image.

This repo deminstrates this simplicity by building a simple "Hello World" binary in the `bin/` directory, from the `hello.go` source file in the `src/` directory. It then will import the `bin/` directory as the root filesystem of a Docker image named `hello` with a Docker tag of `latest`. To see the specifics of how this is done, just look at the `Makefile`.


TO COMPLILE THE BINARY
---

```bash
$ make build-bin
```

TO IMPORT ./bin AS DOCKER IMAGE
---

```bash
$ make import
```

TO BUILD FROM Dockerfile
---

```bash
$ make build-docker
```

TO RUN THE RESULTING DOCKER IMAGE
---
since the `run` target depends on the other stages in the `Makefile` it will make sure those are done before running the docker container.
```bash
$ make run
```

